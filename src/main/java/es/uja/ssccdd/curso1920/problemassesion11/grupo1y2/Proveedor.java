/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion11.grupo1y2;

import static es.uja.ssccdd.curso1920.problemassesion11.grupo1y2.Constantes.BROKER_URL;
import static es.uja.ssccdd.curso1920.problemassesion11.grupo1y2.Constantes.COMPONENTES;
import static es.uja.ssccdd.curso1920.problemassesion11.grupo1y2.Constantes.QUEUE;
import es.uja.ssccdd.curso1920.problemassesion11.grupo1y2.Constantes.TipoComponente;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author pedroj
 */
public class Proveedor implements Runnable {
    private final String iD;
    private final ArrayList<Ordenador> pedido;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination[] destination;

    public Proveedor(String iD) {
        this.iD = iD;
        this.pedido = new ArrayList();
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Comienza con su pedido...");
        
        try {
            inicio();
            
            int indice = 1;
            while(true) {
                prepararPedido(indice);
                indice++;
            }
        } catch (Exception e) {
            System.out.println("TAREA-" + iD + 
                               " Hay una INCIDENCIA en la confección del pedido: " + e.getMessage());
        } finally {
            fin();
            presentarPedido();
            System.out.println("TAREA-" + iD + " Finaliza su ejecución...");
        }   
    }

    public String getiD() {
        return iD;
    }
    
    private void inicio() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
        // Tenemos un destino para cada componente
        destination = new Destination[COMPONENTES.length];
        for(TipoComponente componente : COMPONENTES)
            destination[componente.ordinal()] = session.createQueue(QUEUE + "." + componente.name());
    }
    
    private void prepararPedido(int indice) throws Exception {
        if( Thread.interrupted() )
            throw new InterruptedException();
        
        Ordenador ordenador = new Ordenador();
        ordenador.setiD(iD + "-" + indice);
        
        // Obtenemos los componentes del ordenador de forma síncrona
        for(TipoComponente componente : COMPONENTES) {
            MessageConsumer consumer = session.createConsumer(destination[componente.ordinal()]);
            TextMessage msg = (TextMessage) consumer.receive();
            consumer.close();
            
            System.out.println("TAREA-" + iD + " Obtiene el componente: " + msg.getText());
            ordenador.addComponente(componente);   
        }
        
        // Simulamos el tiempo de construcción de un ordenador para añadirlo al pedido
        TimeUnit.SECONDS.sleep(ordenador.tiempoMontaje());
        System.out.println("TAREA-" + iD + " Finaliza el ordenador: " + indice);
        pedido.add(ordenador);
    }
    
    private void fin() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }
    
    private void presentarPedido() {
        // Presentamos el resultado del pedido
        System.out.println("TAREA-" + iD + " el pedido obtenido es \n______________");
        for( Ordenador ordenador : pedido )
            System.out.println(ordenador.toString());
    }
}
