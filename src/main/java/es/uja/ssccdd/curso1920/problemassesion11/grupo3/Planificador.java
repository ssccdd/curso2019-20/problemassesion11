/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion11.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion11.grupo1y2.Constantes.BROKER_URL;
import static es.uja.ssccdd.curso1920.problemassesion11.grupo1y2.Constantes.QUEUE;
import static es.uja.ssccdd.curso1920.problemassesion11.grupo3.Constantes.EstadoEjecucion.EN_EJECUCION;
import static es.uja.ssccdd.curso1920.problemassesion11.grupo3.Constantes.EstadoEjecucion.FINALIZADO;
import static es.uja.ssccdd.curso1920.problemassesion11.grupo3.Constantes.PROCESOS;
import es.uja.ssccdd.curso1920.problemassesion11.grupo3.Constantes.TipoProceso;
import java.util.concurrent.TimeUnit;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author pedroj
 */
public class Planificador implements Runnable {
    private final String iD;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;

    public Planificador(String iD) {
        this.iD = iD;
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Comienza su ejecución...");
        
        try {
            inicio();
            
            // Simulamos la ejecución del proceso obtenido 
            int indice = 1;
            while(true) {
                ejecutarProceso(indice);
                indice++;
            }
                
        } catch (Exception e) {
            System.out.println("TAREA-" + iD + 
                               " Hay una INCIDENCIA en la ejecución: " + e.getMessage());
        } finally {
            fin();
            System.out.println("TAREA-" + iD + " Finaliza su ejecución...");
        }
    }
    
    private void inicio() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
    }
    
    private void ejecutarProceso(int indice) throws Exception {
        if( Thread.interrupted() )
            throw new InterruptedException();
        
        // Obtenemos el mensaje que representa al proceso a ejecutar
        MessageConsumer consumer = session.createConsumer(destination);
        TextMessage msg = (TextMessage) consumer.receive();
        consumer.close();
        
        // Creamos el proceso para simular su ejecución
        Proceso proceso = new Proceso(indice, TipoProceso.valueOf(msg.getText()));
        proceso.setEstado(EN_EJECUCION);
        System.out.println("TAREA-" + iD + " Ejecuta el proceso: " + proceso);
        TimeUnit.SECONDS.sleep(proceso.tiempoEjecucion());
        proceso.setEstado(FINALIZADO);
    }
    
    private void fin() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }
}
