/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion11.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion11.grupo3.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion11.grupo3.Constantes.NUM_PROCESOS;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import static es.uja.ssccdd.curso1920.problemassesion11.grupo3.Constantes.GESTORES;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso1920.problemassesion11.grupo3.Constantes.PLANIFICADORES;

/**
 *
 * @author pedroj
 */
public class Sesion11 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // TODO code application logic here
        ExecutorService ejecucion;
        List<Future<?>> listaTareas;
        Future<?> tarea;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        ejecucion = Executors.newCachedThreadPool();
        listaTareas = new ArrayList();
        
        // Crear y ejecutar los gestores de procesos
        for(int i = 0; i < GESTORES; i++) {
            GestorProceso gestor;
            gestor = new GestorProceso("Gestor-"+i, NUM_PROCESOS);
            tarea = ejecucion.submit(gestor);
            listaTareas.add(tarea);
        }
        
        // Crear y ejecutar los planificadores
        for(int i = 0; i < PLANIFICADORES; i++) {
            Planificador planificador;
            planificador = new Planificador("Planificador-"+i);
            tarea = ejecucion.submit(planificador);
            listaTareas.add(tarea);
        }
        
        // Esperamos por un tiempo
        System.out.println("HILO(Principal) SUSPENDIDO POR UN TIEMPO");
        TimeUnit.MINUTES.sleep(TIEMPO_ESPERA);
        
        // Se solicita la cancelación de los pedidos que han excedido el tiempo
        System.out.println("HILO(Principal) Solicita la finalización de los pedidos");
        for( Future<?> task : listaTareas ) 
            task.cancel(true);
        
        // Finalizamos el ejecutor y esperamos a que todas las tareas finalicen
        System.out.println("HILO(Principal) Espera a la finalización de las tareas");
        ejecucion.shutdown();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
