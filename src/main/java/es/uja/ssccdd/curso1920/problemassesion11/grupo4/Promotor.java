/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion11.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion11.grupo1y2.Constantes.BROKER_URL;
import static es.uja.ssccdd.curso1920.problemassesion11.grupo1y2.Constantes.QUEUE;
import static es.uja.ssccdd.curso1920.problemassesion11.grupo4.Constantes.SENSORES;
import es.uja.ssccdd.curso1920.problemassesion11.grupo4.Constantes.TipoCasa;
import es.uja.ssccdd.curso1920.problemassesion11.grupo4.Constantes.TipoSensor;
import static es.uja.ssccdd.curso1920.problemassesion11.grupo4.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author pedroj
 */
public class Promotor implements Runnable {
    private final String iD;
    private final int numCasas;
    private final ArrayList<Casa> instalacion;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination[] destination;

    public Promotor(String iD, int numCasas) {
        this.iD = iD;
        this.numCasas = numCasas;
        this.instalacion = new ArrayList();
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " Comienza con su instalación...");
        
        try {
            inicio();
            
            for(int i = 0; i < numCasas; i++)
                realizarInstalacion(i);
        } catch (Exception e) {
            System.out.println("TAREA-" + iD + 
                               " Hay una INCIDENCIA en la instalación: " + e.getMessage());
        } finally {
            fin();
            presentarInstalacion();
            System.out.println("TAREA-" + iD + " Finaliza su ejecución...");
        }   
    }
    
    private void inicio() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
        // Tenemos un destino para cada componente
        destination = new Destination[SENSORES.length];
        for(TipoSensor sensor : SENSORES)
            destination[sensor.ordinal()] = session.createQueue(QUEUE + "." + sensor.name());
    }
    
    private void realizarInstalacion(int indice) throws Exception {
        if( Thread.interrupted() )
            throw new InterruptedException();
        
        // Generamos una casa aleatoria
        Casa casa = new Casa(indice,TipoCasa.getCasa());
        // Generamos las habitaciones a instalar de forma aleatoria
        int numHabitaciones = aleatorio.nextInt(casa.getNumHabitaciones()) + 1;
        for(int i = 0; i < numHabitaciones; i++)
            for(TipoSensor tipoSensor : SENSORES) {
                // Obtenemos el sensor que hay que instalar
                MessageConsumer consumer = session.createConsumer(destination[tipoSensor.ordinal()]);
                TextMessage msg = (TextMessage) consumer.receive();
                consumer.close();
                System.out.println("TAREA-" + iD + " Obtiene el sensor: " + msg.getText());
                
                // Se crea el sensor
                Sensor sensor = new Sensor(tipoSensor.hashCode(), tipoSensor);
                casa.addSensor(sensor);
                // Simula el tiempo de instalación
                System.out.println("TAREA-" + iD + " Instala el " + sensor);
                TimeUnit.SECONDS.sleep(sensor.getTiempoInstalacion());
            }
        
        instalacion.add(casa);
    }
    
    private void fin() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }
    
    private void presentarInstalacion() {
        // Presentamos el resultado del pedido
        System.out.println("TAREA-" + iD + " las casas instaladas son\n______________");
        for( Casa casa : instalacion )
            System.out.println(casa.toString());
    }
}
