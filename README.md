[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 11

Problemas propuestos para la Sesión11 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2019-20.

Los ejercicios son diferentes para cada grupo:

- [Grupo 1 y 2](https://gitlab.com/ssccdd/curso2019-20/problemassesion11#grupo-1-y-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2019-20/problemassesion11#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2019-20/problemassesion11#grupo-4)

## Grupo 1 y 2

El ejercicio consiste en la realización de unos pedidos de ordenadores por parte de una número de proveedores. Cada uno de los proveedores recogen los componentes desde una localización conocida para cada uno de ellos para la construcción de los ordenadores de su pedido.  Los componentes del ordenador tienen un fabricante asociado para su producción en la localización para los proveedores. La finalización de los pedidos tiene una fecha tope de entrega. Alcanzada la fecha de entrega los proveedores muestran la lista de los ordenadores que ha completado. La herramienta de Java para la realización del ejercicio es **JMS**, por medio de una conexión **P2P** y con recepción **síncrona** de los mensajes, y el *broker* [ActiveMQ](http://activemq.apache.org/). Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución. Se tiene que completar la constante `QUEUE` con la cadena `"uja.ssccdd.sesion11.USUARIO_DE_ALUMNO_UJA"`. De esta forma cada alumno tendrá asociados sus propios destinos en el *broker*.
- `Ordenador`: Representa el elemento que se debe construir con cada uno de los `TipoComponente`.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan. Las tareas deben diseñarse suponiendo que no se comparte memoria entre ellas:

- `Frabricante` : Simula la construcción de un `TipoCompoente` asociado. La tarea tiene como variables de instancia un identificador, un `TipoComponente` y un número de unidades que fabricará de ese componente.
	- Inicia la conexión con el *broker* con las constantes de configuración que hay en la interface `Constantes`. El destino donde enviará los mensajes debe estar dedicado al componente que fabrica y se concatenará la la constante `QUEUE`.
	- Fabrica el número de componentes asignados en su construcción.
		- Se crea un productor al destino donde se envían los mensajes.
		- El mensaje tendrá como contenido el nombre del componente asociado.
		- Para cada componente se simulará su tiempo de fabricación antes de simular la fabricación del siguiente componente.
	- Finaliza la conexión con el *broker* antes de finalizar su ejecución. Hay que tener previstas posibles excepciones que finalicen la ejecución de la tarea de forma anticipada.

- `Proveedor` : Simula la construcción de unos ordenadores para un pedido. Para la construcción de los ordenadores utiliza las piezas que se han fabricado por los `Fabricante`. Las variables de instancia son un identificador y una lista de ordenadores que conformarán el pedido. La lista inicialmente estará vacía.
	- Inicia la conexión con el *broker* con las constantes de configuración que hay en la interface `Constantes`. Hay que definir una lista de destinos, una para cada uno de los componentes, donde se recogen los componentes fabricados.
	- Mientas no se pida la finalización se crearán ordenadores.
		- Para cada ordenador del pedido.
			- Se va recogiendo un mensaje de forma síncrona del destino asociado para cada uno de los componentes que forman el ordenador.
			- Cuando se han recogido todos los componentes se simulará el tiempo de montaje asociado al ordenador antes de añadirlo a su lista de pedidos.
	- Finaliza la conexión con el *broker* antes de finalizar y se presenta la lista del pedido del proveedor. Hay que tener previstas posibles excepciones que finalicen la ejecución de la tarea de forma anticipada. La lista del pedido del proveedor debe presentarse independientemente de la finalización de la tarea.

- `Hilo principal` : Realizará los siguientes pasos:
	- Una lista de `Future<?>` para la finalización de las tareas.
	- Se crea un marco de ejecución con un número indeterminado de hilos asociado.
	- Se crean y ejecutan tantas tareas `Fabricante` como `TipoComponente` disponibles en la interface `Constantes`. El número de unidades está recogido por la constante `UNIDADES`.
	- Se crean y ejecutan un número `PROVEEDORES` de tareas `Proveedor`.
	- Se suspende la ejecución por un `TIEMPO_ESPERA` minutos.
	- Finalizan las tareas activas y espera su finalización para concluir.

## Grupo 3

El ejercicio consiste en la simulación de ejecución de procesos que han sido creados y almacenados en un destino conocido. El tiempo de ejecución tiene un plazo establecido para la finalización de los procesos. Alcanzado el tiempo límite los procesos activos serán interrumpidos. La herramienta de Java para la realización del ejercicio es **JMS**, por medio de una conexión **P2P** y con recepción **síncrona** de los mensajes, y el *broker* [ActiveMQ](http://activemq.apache.org/). Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución. Se tiene que completar la constante `QUEUE` con la cadena `"uja.ssccdd.sesion11.USUARIO_DE_ALUMNO_UJA.procesos"`. De esta forma cada alumno tendrá asociados sus propios destinos en el *broker*.
- `Proceso`: Representa al proceso que simulará su ejecución en una unidad de procesamiento.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan. Las tareas deben diseñarse suponiendo que no se comparte memoria entre ellas:

- `GestorProceso` : Simula la creación de un proceso para su planificación más adelante. Las variables de instancia son un identificador y un número de procesos que creará.
	- Inicia la conexión con el *broker* con las constantes de configuración que hay en la interface `Constantes`. Todos los procesos se envían al mismo destino para su planificación.
	- Hasta que no haya finalizado la creación de los procesos asignados
		- Crea un `TipoProceso` aleatorio simulando un `TIEMPO_GESTION`.
		- Se envía al destino un mensaje para simular la creación el proceso para su planificación. El contenido del mensaje es el nombre del `TipoProceso`.
	- Finaliza la conexión con el *broker* antes de finalizar su ejecución. Hay que tener previstas posibles excepciones que finalicen la ejecución de la tarea de forma anticipada.
	
- `Planificador` : Simulará la ejecución de procesos que se obtienen de un destino conocido. Como variable de instancia tiene un identificador.
	- Inicia la conexión con el *broker* con las constantes de configuración que hay en la interface `Constantes`. Todos los procesos se envían al mismo destino para su planificación.
	- Mientras no se solicite la finalización.
		- Se recoge el mensaje del destino, de forma síncrona, y con el contenido del mensaje se busca el `TipoProceso` que coincida por su nombre y se crea el `Proceso` asociado.
		- Se simula el tiempo de ejecución del proceso y se muestra un mensaje indicándolo. 
	- Finaliza la conexión con el *broker* antes de finalizar su ejecución. Hay que tener previstas posibles excepciones que finalicen la ejecución de la tarea de forma anticipada.

- `Hilo principal` : Realizará los siguientes pasos:
	- Una lista de `Future<?>` para la finalización de las tareas.
	- Se crea un marco de ejecución con un número indeterminado de hilos asociado.
	- Se crean y ejecutan un número `GESTORES` de tareas `GestorProceso` con un número de procesos recogido en la constante `NUM_PROCESOS`.
	- Se crean y ejecutan un número `PLANIFICADORES` de tareas `Planificador`.
	- Se suspende la ejecución por un `TIEMPO_ESPERA` minutos.
	- Finalizan las tareas activas y espera su finalización para concluir.

## Grupo 4

El ejercicio consiste en la instalación de sensores en un número de casas asociadas a un promotor. Cada uno de los promotores recogen los sensores desde una localización conocida para cada uno de ellos para para la instalación en una casa.  Los sensores tienen un fabricante asociado para su producción en la localización para los promotores. La finalización de la instalación tiene una fecha tope. Alcanzada la fecha tope los promotores muestran la lista de casas que ha completado. La herramienta de Java para la realización del ejercicio es **JMS**, por medio de una conexión **P2P** y con recepción **síncrona** de los mensajes, y el *broker* [ActiveMQ](http://activemq.apache.org/). Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución. Se tiene que completar la constante `QUEUE` con la cadena `"uja.ssccdd.sesion11.USUARIO_DE_ALUMNO_UJA"`. De esta forma cada alumno tendrá asociados sus propios destinos en el *broker*.
- `Sensor`: Simula un tipo de sensor y también dispone de un método que simulará el tiempo necesario para su instalación.
- `Casa`: Simula donde se instalará un sensor. Dispone de una distribución de habitaciones según el `TipoCasa` que viene definido en `Constantes`.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan. Las tareas deben diseñarse suponiendo que no se comparte memoria entre ellas:

- `Frabricante` : Simula la construcción de un `TipoSensor` asociado. La tarea tiene como variables de instancia un identificador, un `TipoSensor` y un número de unidades que fabricará de ese sensor.
	- Inicia la conexión con el *broker* con las constantes de configuración que hay en la interface `Constantes`. El destino donde enviará los mensajes debe estar dedicado al sensor que fabrica y se concatenará la la constante `QUEUE`.
	- Fabrica el número de sensores asignados en su construcción.
		- Se crea un productor al destino donde se envían los mensajes.
		- El mensaje tendrá como contenido el nombre del sensor asociado.
		- Para cada sensor se simulará su tiempo de fabricación antes de simular la fabricación del siguiente sensor.
	- Finaliza la conexión con el *broker* antes de finalizar su ejecución. Hay que tener previstas posibles excepciones que finalicen la ejecución de la tarea de forma anticipada.

- `Promotor` : Simula la instalación de sensores en una lista de casas. Para la instalación de las casas utiliza los sensores que se han fabricado por los `Fabricante`. Las variables de instancia son un identificador un número de casas y una lista de casas instaladas. La lista inicialmente estará vacía.
	- Inicia la conexión con el *broker* con las constantes de configuración que hay en la interface `Constantes`. Hay que definir una lista de destinos, una para cada uno de los sensores, donde se recogen los sensores fabricados.
	- Mientras no se han instalado todas las casas asignadas
		- Para cada casa se genera un número aleatorio de habitaciones que se instalarán. 
			- Para cada habitación a instalar.
				- Se va recogiendo un mensaje de forma síncrona del destino asociado para cada uno de los sensores que se instalarán. Se simula el tiempo de instalación asociado al sensor.
			- Cuando se han instalado todas las habitaciones se añade la casa a la lista de casas instaladas.
	- Finaliza la conexión con el *broker* antes de finalizar y se presenta la lista de casas instaladas. Hay que tener previstas posibles excepciones que finalicen la ejecución de la tarea de forma anticipada. La lista de casas instaladas debe presentarse independientemente de la finalización de la tarea.

- `Hilo principal` : Realizará los siguientes pasos:
	- Una lista de `Future<?>` para la finalización de las tareas.
	- Se crea un marco de ejecución con un número indeterminado de hilos asociado.
	- Se crean y ejecutan tantas tareas `Fabricante` como `TipoSensor` disponibles en la interface `Constantes`. El número de unidades está recogido por la constante `NUM_SENSORES`.
	- Se crean y ejecutan un número `PROMOTORES` de tareas `Promotor` con un número de casas está recogido por la constante `NUM_CASAS`.
	- Se suspende la ejecución por un `TIEMPO_ESPERA` minutos.
	- Finalizan las tareas activas y espera su finalización para concluir.

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE0MjMzNzg0MDcsLTE5NDg3MDgxMzIsLT
EyNDY5NjQ3OF19
-->